//
//  SetupViewController.swift
//  MVLPricingChart
//
//  Created by Nguyen Hong Phat on 11/18/18.
//  Copyright © 2018 Nguyen Hong Phat. All rights reserved.
//

import UIKit
import DropDown

class SetupViewController: BaseViewController {

    @IBOutlet weak var pickupZoneView: UIView!
    @IBOutlet weak var pickupZoneLabel: UILabel!
    @IBOutlet weak var destinationZoneView: UIView!
    @IBOutlet weak var destinationZoneLabel: UILabel!
    
    @IBOutlet weak var distanceTypeView: UIView!
    @IBOutlet weak var distanceTypeLabel: UILabel!
    @IBOutlet weak var dateTypeView: UIView!
    @IBOutlet weak var dateTypeLabel: UILabel!
    
    @IBOutlet weak var drawChatButtonView: UIView!
    @IBOutlet weak var drawChatButton: UIButton!

    //MARK: DROPDOWN
    let dropDownPickup = DropDown()
    let dropDownDestination = DropDown()
    let dropDownDistanceType = DropDown()
    let dropDownDateType = DropDown()
    
    var pickupZoneType = ZoneType.veryHigh
    var destinationZoneType = ZoneType.veryHigh
    var distanceType = DistanceType.lessThan5
    var dateType = DateType.weekdays
    
    let zoneTypes: [ZoneType] = [.veryHigh, .high, .low, .veryLow]
    let distanceTypes: [DistanceType] = [.lessThan5, .form5To10, .from10To20]
    let dateTypes: [DateType] = [.weekdays, .weekend]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MVL Pricing Research"
        [pickupZoneView, destinationZoneView, distanceTypeView, dateTypeView].forEach({
            $0?.backgroundColor = UIColor(rgb: 0xece8d9)
            $0?.dropShadow(width: 2.0, height: 5.0, opacity: 0.2, cornerRadius: .slight)
        })
        drawChatButtonView.backgroundColor = UIColor(rgb: 0x494949)
        drawChatButton.setTitleColor(UIColor(rgb: 0xece8d9), for: .normal)
        drawChatButtonView.dropShadow(width: 4.0, height: 5.0, opacity: 0.3, cornerRadius: .rounded)
        
        dropDownPickup.dataSource = ["Very High", "High", "Low", "Very Low"]
        dropDownDestination.dataSource = ["Very High", "High", "Low", "Very Low"]
        dropDownDistanceType.dataSource = ["Less than 5km", "From 5km to 10km", "From 10km to 20km"]
        dropDownDateType.dataSource = ["Weekdays", "Weekend"]
        
        let tapGesturePickup = UITapGestureRecognizer(target: self, action: #selector(showDropdownPickup))
        self.pickupZoneView.addGestureRecognizer(tapGesturePickup)
        
        let tapGestureDestination = UITapGestureRecognizer(target: self, action: #selector(showDropdownDestination))
        self.destinationZoneView.addGestureRecognizer(tapGestureDestination)
        
        let tapGestureDistance = UITapGestureRecognizer(target: self, action: #selector(showDropdownDistanceType))
        self.distanceTypeView.addGestureRecognizer(tapGestureDistance)
        
        let tapGestureDate = UITapGestureRecognizer(target: self, action: #selector(showDropdownDateType))
        self.dateTypeView.addGestureRecognizer(tapGestureDate)
        
        pickupZoneLabel.text = "Very High"
        destinationZoneLabel.text = "Very High"
        distanceTypeLabel.text = "Less than 5km"
        dateTypeLabel.text = "Weekdays"
        
        dropDownPickup.anchorView = self.pickupZoneView
        dropDownPickup.selectionAction = { [unowned self] (index: Int, item: String) in
            self.pickupZoneType = self.zoneTypes[index]
            self.pickupZoneLabel.text = item
            self.dropDownPickup.hide()
        }
        
        dropDownDestination.anchorView = self.destinationZoneView
        dropDownDestination.selectionAction = { [unowned self] (index: Int, item: String) in
            self.destinationZoneType = self.zoneTypes[index]
            self.destinationZoneLabel.text = item
            self.dropDownDestination.hide()
        }
        
        dropDownDistanceType.anchorView = self.distanceTypeView
        dropDownDistanceType.selectionAction = { [unowned self] (index: Int, item: String) in
            self.distanceType = self.distanceTypes[index]
            self.distanceTypeLabel.text = item
            self.dropDownDistanceType.hide()
        }
        
        dropDownDateType.anchorView = self.dateTypeView
        dropDownDateType.selectionAction = { [unowned self] (index: Int, item: String) in
            self.dateType = self.dateTypes[index]
            self.dateTypeLabel.text = item
            self.dropDownDateType.hide()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpNavigationBarWithLine()
    }
    
    @objc func showDropdownPickup(){
        dropDownPickup.show()
    }
    
    @objc func showDropdownDestination(){
        dropDownDestination.show()
    }
    
    @objc func showDropdownDistanceType(){
        dropDownDistanceType.show()
    }
    
    @objc func showDropdownDateType(){
        dropDownDateType.show()
    }
    
    @IBAction func onDrawChart(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chartViewVC") as! ViewController
        vc.pickupZoneType = self.pickupZoneType
        vc.destinationZoneType = self.destinationZoneType
        vc.distanceType = self.distanceType
        vc.dateType = self.dateType
        navigationController?.pushViewController(vc, animated: true)
    }
}
