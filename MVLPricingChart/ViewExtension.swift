//
//  ViewExtension.swift
//  MVLPricingChart
//
//  Created by Nguyen Hong Phat on 11/18/18.
//  Copyright © 2018 Nguyen Hong Phat. All rights reserved.
//

import UIKit

enum CornerRadius{
    case none
    case rounded
    case slight
}

extension UIView{
    func dropShadow(width: CGFloat, height: CGFloat, opacity: Float, cornerRadius: CornerRadius){
        switch cornerRadius {
        case .slight:
            self.layer.cornerRadius = 5.0
            break
        case .rounded:
            self.layer.cornerRadius = self.frame.height * 0.5
            break
        default:
            self.layer.cornerRadius = 0.0
            break
        }
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowOpacity = opacity
    }
    
    func makeLightCorners(){
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        dropShadow(width: 0.0, height: 4.0, opacity: 0.4, cornerRadius: .slight)
    }
}

