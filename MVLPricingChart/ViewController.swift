//
//  ViewController.swift
//  MVLPricingChart
//
//  Created by Nguyen Hong Phat on 11/17/18.
//  Copyright © 2018 Nguyen Hong Phat. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    @IBOutlet weak var lineC: LineChart!
    var researchs: [Research] = []
    var data: [CGFloat] = [0,0,0,0,0]
    private let hoursWeekdays = ["6:10", "7:10", "7:40", "8:10", "8:40", "9:10", "10:10", "11:10","11:40", "12:10", "12:40", "1:10", "2:10", "3:10", "4:10", "5:10", "5:40", "6:10", "6:40", "7:10", "8:10", "9:10", "10:10"]
    private let hoursWeekend = ["6:10", "7:10", "8:10", "9:10", "9:40", "10:10", "10:40", "11:10", "12:10", "1:10", "2:10", "3:10", "4:10", "4:40", "5:10", "5:40", "6:10", "6:40", "7:10", "8:10", "9:10", "10:10"]
    let grab1Color = UIColor(rgb: 0x488b8f)
    let grab2Color = UIColor(rgb: 0xadd2c9)
    let vatoColor = UIColor(rgb: 0xf05d23)
    
    var pickupZoneType: ZoneType?
    var destinationZoneType: ZoneType?
    var distanceType: DistanceType?
    var dateType: DateType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = getTitle()
        
        view.backgroundColor = UIColor(rgb: 0xece8d9)
        
        let content = readCSVFile()
        let values = content.split(separator: "\r\n")
        loadInput(values: values)
    
        setUpChart(dateType: dateType ?? .weekdays)
        fetchData(pickupZone: pickupZoneType, destinationZone: destinationZoneType, distanceType: distanceType, dateType: dateType)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpNavigationBarWithLine()
    }
    
    func getTitle() -> String{
        var title = ""
        
        switch pickupZoneType {
        case .veryHigh?:
            title += "Very High"
        case .high?:
            title += "High"
        case .low?:
            title += "Low"
        case .veryLow?:
            title += "Very Low"
        default:
            title += ""
        }
        
        title += " - "
        
        switch destinationZoneType {
        case .veryHigh?:
            title += "Very High"
        case .high?:
            title += "High"
        case .low?:
            title += "Low"
        case .veryLow?:
            title += "Very Low"
        default:
            title += ""
        }
        
        switch distanceType {
        case .lessThan5?:
            title += " (less than 5km)"
        case .form5To10?:
            title += " (from 5km to 10km)"
        case .from10To20?:
            title += " (from 10km to 20km)"
        default:
            title += ""
        }
        
        switch dateType {
        case .weekdays?:
            title += " - Weekdays"
        case .weekend?:
            title += " - Weekend"
        default:
            title += ""
        }
        
        return title
    }
    
    func fetchData(pickupZone: ZoneType?, destinationZone: ZoneType?, distanceType: DistanceType?, dateType: DateType?) {
        if let mySearch = filter(pickupZone: pickupZone, destinationZone: destinationZone, distanceType: distanceType, vehicleType: .grab1, dateType: dateType) {
            if let id = mySearch.id, let prices = mySearch.pickDate?.prices {
                debugPrint("Found id: \(id) with real fare: \(prices)")
                let floats = prices.map{Float($0)!}
                let cgfloats = floats.map{CGFloat($0)}
                lineC.clear()
                lineC.addLine(cgfloats)
            }
        } else {
            debugPrint("Not found")
        }


        if let mySearch = filter(pickupZone: pickupZone, destinationZone: destinationZone, distanceType: distanceType, vehicleType: .grab2, dateType: dateType) {
            if let id = mySearch.id, let prices = mySearch.pickDate?.prices {
                debugPrint("Found id: \(id) with real fare: \(prices)")
                let floats = prices.map{Float($0)!}
                let cgfloats = floats.map{CGFloat($0)}
                lineC.addLine(cgfloats)
            }
        } else {
            debugPrint("Not found")
        }
        
        if let mySearch = filter(pickupZone: pickupZone, destinationZone: destinationZone, distanceType: distanceType, vehicleType: .vato, dateType: dateType) {
            if let id = mySearch.id, let prices = mySearch.pickDate?.prices {
                debugPrint("Found id: \(id) with real fare: \(prices)")
                let floats = prices.map{Float($0)!}
                let cgfloats = floats.map{CGFloat($0)}
                lineC.addLine(cgfloats)
            }
        } else {
            debugPrint("Not found")
        }
        
        if dateType == .weekdays{
            lineC.colors = [grab1Color, grab2Color, vatoColor]
        } else {
            lineC.colors = [grab1Color, vatoColor, grab2Color]
        }
    }
    
    func getcgFloatValues(prices: [String]) -> [CGFloat?] {
        let floats = prices.map{Float($0)!}
        let cgfloats = floats.map{CGFloat($0)}
        return cgfloats
    }
    
    func setUpChart(dateType: DateType){
        lineC.animation.enabled = true
        lineC.area = false
        lineC.x.labels.visible = true
        lineC.x.grid.visible = false
        lineC.x.grid.count = 5
        lineC.y.grid.count = 5
        if dateType == .weekdays{
            lineC.x.labels.values = hoursWeekdays
        } else {
            lineC.x.labels.values = hoursWeekend
        }
        lineC.y.labels.visible = true
        lineC.y.grid.visible = false
        lineC.addLine(data)
        lineC.translatesAutoresizingMaskIntoConstraints = false
        lineC.backgroundColor = UIColor.clear
    }
    
    func filter(pickupZone: ZoneType?, destinationZone: ZoneType?, distanceType: DistanceType?, vehicleType: VehicleType?, dateType: DateType?) -> Research?{
        if dateType == .weekend, vehicleType == .grab2 {
            return nil
        }
        
        for research in researchs {
            if research.pickupZone == pickupZone, research.destinationZone == destinationZone, research.distanceType == distanceType, research.vehicleType == vehicleType, research.pickDate?.dateType == dateType {
                return research
            }
        }
        return nil
    }
    
    func loadInput(values: [String.SubSequence]) {
        let rows = values.count
        debugPrint(rows)
        for i in 4...rows-1 {
            let rowData = values[i]
            let cells = rowData.split(separator: ",", maxSplits: Int.max, omittingEmptySubsequences: false)
            //debugPrint("Row first: \(cells.first!) has total cells - \(cells.count), last value \(cells.last!)")
            
            let id = Int(cells[0])
            let person = String(cells[1])
            let districtType = convertToDistrictType(district: String(cells[2]))
            let origin = String(cells[3])
            let pickupZone = convertToZone(zone: String(cells[4]))
            let end = String(cells[5])
            let destinationZone = convertToZone(zone: String(cells[6]))
            let actualDistance = Float(cells[7])
            let distanceType = convertToDistanceType(distance: String(cells[8]))
            let baseTADAFare = Float(cells[9])
            let baseGRABFare = Float(cells[10])
            let baseVATOFare = Float(cells[11])
            
            //Weekdays
            let researchGrab1WD = Research(id: id, person: person, districtType: districtType, origin: origin, end: end, vehicleType: .grab1, baseTADAFare: baseTADAFare, baseGRABFare: baseGRABFare, baseVATOFare: baseVATOFare, pickupZone: pickupZone, destinationZone: destinationZone, distanceType: distanceType, actualDistance: actualDistance, pickDate: nil)
            let researchGrab2WD = Research(id: id, person: person, districtType: districtType, origin: origin, end: end, vehicleType: .grab2, baseTADAFare: baseTADAFare, baseGRABFare: baseGRABFare, baseVATOFare: baseVATOFare, pickupZone: pickupZone, destinationZone: destinationZone, distanceType: distanceType, actualDistance: actualDistance, pickDate: nil)
            let researchVatoWD = Research(id: id, person: person, districtType: districtType, origin: origin, end: end, vehicleType: .vato, baseTADAFare: baseTADAFare, baseGRABFare: baseGRABFare, baseVATOFare: baseVATOFare, pickupZone: pickupZone, destinationZone: destinationZone, distanceType: distanceType, actualDistance: actualDistance, pickDate: nil)
            
            let pickDateWDGrab1 = PickDate(dateType: .weekdays, prices: [])
            let pickDateWDGrab2 = PickDate(dateType: .weekdays, prices: [])
            let pickDateWDVato = PickDate(dateType: .weekdays, prices: [])
            
            for ii in 12...80 {
                if ii % 3 == 0 {
                    pickDateWDGrab1.prices.append(String(cells[ii]))
                } else if ii % 3 == 1 {
                    pickDateWDGrab2.prices.append(String(cells[ii]))
                } else {
                    pickDateWDVato.prices.append(String(cells[ii]))
                }
            }
            researchGrab1WD.pickDate = pickDateWDGrab1
            researchGrab2WD.pickDate = pickDateWDGrab2
            researchVatoWD.pickDate = pickDateWDVato
            
            researchs.append(researchGrab1WD)
            researchs.append(researchGrab2WD)
            researchs.append(researchVatoWD)
            
            //Weekend
            let researchGrabWK = Research(id: id, person: person, districtType: districtType, origin: origin, end: end, vehicleType: .grab1, baseTADAFare: baseTADAFare, baseGRABFare: baseGRABFare, baseVATOFare: baseVATOFare, pickupZone: pickupZone, destinationZone: destinationZone, distanceType: distanceType, actualDistance: actualDistance, pickDate: nil)
            let researchVatoWK = Research(id: id, person: person, districtType: districtType, origin: origin, end: end, vehicleType: .vato, baseTADAFare: baseTADAFare, baseGRABFare: baseGRABFare, baseVATOFare: baseVATOFare, pickupZone: pickupZone, destinationZone: destinationZone, distanceType: distanceType, actualDistance: actualDistance, pickDate: nil)
            
            let pickDateWKGrab = PickDate(dateType: .weekend, prices: [])
            let pickDateWKVato = PickDate(dateType: .weekend, prices: [])
            
            for ii in 81...124 {
                if ii % 2 == 1 {
                    pickDateWKGrab.prices.append(String(cells[ii]))
                } else {
                    pickDateWKVato.prices.append(String(cells[ii]))
                }
            }
            researchGrabWK.pickDate = pickDateWKGrab
            researchVatoWK.pickDate = pickDateWKVato
            researchs.append(researchGrabWK)
            researchs.append(researchVatoWK)
        }
    }
    
    func convertToDistrictType(district: String) -> DistrictType? {
        if district == "1" {
            return DistrictType.district1
        } else if district == "2" {
            return DistrictType.district2
        } else if district == "3" {
            return DistrictType.district3
        } else if district == "4" {
            return DistrictType.district4
        } else if district == "5" {
            return DistrictType.district5
        } else if district == "6" {
            return DistrictType.district6
        } else if district == "7" {
            return DistrictType.district7
        } else if district == "8" {
            return DistrictType.district8
        } else if district == "9" {
            return DistrictType.district9
        } else if district == "10" {
            return DistrictType.district10
        } else if district == "11" {
            return DistrictType.district11
        } else if district == "12" {
            return DistrictType.district12
        } else if district == "12" {
            return DistrictType.district12
        } else if district == "Binh Thanh" {
            return DistrictType.binhthanh
        } else if district == "Phu Nhuan" {
            return DistrictType.phunhuan
        } else if district == "Tan Binh" {
            return DistrictType.tanbinh
        } else if district == "Go Vap" {
            return DistrictType.govap
        } else if district == "Thu Duc" {
            return DistrictType.thuduc
        } else {return nil}

    }
    
    func convertToZone(zone: String) -> ZoneType? {
        if zone == "Very High - 1 -3" {
            return ZoneType.veryHigh
        } else if zone == "High - 4 -5 -10 - Phu Nhuan - Binh Thanh" {
            return ZoneType.high
        } else if zone == "Low - 2 - 6 -7 -8 -11 - Tan Binh - Go Vap" {
            return ZoneType.low
        } else if zone == "Very Low - 9 -12 -Thu Duc" {
            return ZoneType.veryLow
        } else {return nil}
    }
    
    func convertToDistanceType(distance: String) -> DistanceType? {
        if distance == "<5 km" {
            return DistanceType.lessThan5
        } else if distance == "5-10 km" {
            return DistanceType.form5To10
        } else if distance == "10-20 km" {
            return DistanceType.from10To20
        } else {return nil}
    }


    func readCSVFile() -> String{
        let file = Bundle.main.path(forResource: "Pricing research", ofType: "csv")
        let url = URL(fileURLWithPath: file!)
        let content = try! String(contentsOf: url, encoding: String.Encoding.utf8)
        return content
    }
}

//ObjectWeekDay
//1. Type .grab1 .grab2? .vato
//2. Base Fare :
//3. Real fare - result
//4. Pickup zone - require
//5. Des zone - require
//6. Distance - require
//7. Date type - .weekday, .weekend
enum DistrictType {
    case district1, district2, district3, district4, district5, district6, district7, district8, district9, district10, district11, district12
    case govap, binhthanh, thuduc, phunhuan, tanphu, binhtan, tanbinh
    case nhabe, binhchanh, hocmon, cuchi, cangio
}

enum VehicleType {
    case grab1, grab2, vato
}

enum DateType {
    case weekdays, weekend
}

enum ZoneType {
    case veryLow, low, high, veryHigh
}

enum DistanceType {
    case lessThan5, form5To10, from10To20
}

class Research {
    let id: Int?
    let person: String?
    let districtType: DistrictType?
    let origin: String?
    let end: String?
    var vehicleType: VehicleType?
    let baseTADAFare: Float?
    let baseGRABFare: Float?
    let baseVATOFare: Float?
    let pickupZone: ZoneType?
    let destinationZone: ZoneType?
    let distanceType: DistanceType?
    let actualDistance: Float?
    var pickDate: PickDate?
    
    init(id: Int?, person: String?, districtType: DistrictType?, origin: String?, end: String?, vehicleType : VehicleType?, baseTADAFare: Float?, baseGRABFare: Float?, baseVATOFare: Float?, pickupZone: ZoneType?, destinationZone: ZoneType?, distanceType: DistanceType?, actualDistance: Float?, pickDate: PickDate?) {
        self.id = id
        self.person = person
        self.districtType = districtType
        self.origin = origin
        self.end = end
        self.vehicleType = vehicleType
        self.baseTADAFare = baseTADAFare
        self.baseGRABFare = baseGRABFare
        self.baseVATOFare = baseVATOFare
        self.pickupZone = pickupZone
        self.destinationZone = destinationZone
        self.distanceType = distanceType
        self.actualDistance = actualDistance
        self.pickDate = pickDate
    }
}

class PickDate {
    var dateType: DateType?
    var prices: [String] = []
    
    init(dateType: DateType?, prices: [String]) {
        self.dateType = dateType
        self.prices = prices
    }
}
