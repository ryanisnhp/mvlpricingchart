//
//  BaseViewController.swift
//  MVLPricingChart
//
//  Created by Nguyen Hong Phat on 11/18/18.
//  Copyright © 2018 Nguyen Hong Phat. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func setUpNavigationBarWithLine(){
        self.navigationController?.navigationBar.barTintColor = UIColor(rgb: 0xece8d9)
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x494949)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x494949),
                                                                        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20.0, weight: .bold)]
    }
}
